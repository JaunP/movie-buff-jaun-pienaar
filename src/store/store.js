import axios from "axios";
import Vuex from "vuex";
import VuexPersistence from 'vuex-persist'

const store = new Vuex.Store({
  state: {
    movieBuffList:[],
    movieBuffKey: 'b44e753d',
    movieFavorites: []
  },
  getters: {
    getMovieList(state) {
      return state.movieBuffList
    },
    getMovieFavorites(state) {
      return state.movieFavorites
    }
  },
  mutations: {
    setMovieBuffList(state, payload) {
      return state.movieBuffList = payload
    },
    addToFavorite(state, payload) {
      return state.movieFavorites.push(payload)
    },
    removeFromFavorite( state, payload){
      const index = state.movieFavorites.indexOf(payload)

      if (index > -1) {
        state.movieFavorites.splice(index, 1);
      }
    }
  },
  actions: {
    async getMovieBuffList(context, params) {
      let url = ''

      if (params.searchPhrase) {
        url = `http://www.omdbapi.com/?apikey=${params.movieBuffKey}&s=${params.searchPhrase}`
      } else {
        url = `http://www.omdbapi.com/?apikey=${params.movieBuffKey}`
      }

      axios.get(url)
      .then(movieList => {
        context.commit("setMovieBuffList", movieList.data.Search)
      })
    },
    addToFavorite(context, movieDetail) {
      context.commit("addToFavorite", movieDetail)
    },
    removeFavorite(context, movieDetail) {
      context.commit("removeFromFavorite", movieDetail)
    }
  },
  plugins: [new VuexPersistence().plugin]
});
export default store;
