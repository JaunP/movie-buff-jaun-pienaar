module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
    { src: '~/plugins/vuex-persist', ssr: false }
  ]
}
